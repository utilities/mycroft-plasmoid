# Translation of plasma_applet_org.kde.plasma.mycroftplasmoid.po to Brazilian Portuguese
# Copyright (C) 2018-2019 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2018, 2019.
# André Marcelo Alvarenga <alvarenga@kde.org>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.mycroftplasmoid\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:41+0000\n"
"PO-Revision-Date: 2019-11-26 22:13-0300\n"
"Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>\n"
"Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 19.04.3\n"

#: contents/config/config.qml:25
#, kde-format
msgid "General"
msgstr "Geral"

#: contents/ui/BottomBarViewComponent.qml:71
#, kde-format
msgid "Enter Query or Say 'Hey Mycroft'"
msgstr "Digite a pergunta ou diga 'Ei Mycroft'"

#: contents/ui/config/configGeneral.qml:45
#, kde-format
msgid "Websocket Address:"
msgstr "Endereço do websocket:"

#: contents/ui/config/configGeneral.qml:53
#, kde-format
msgid "Additional Settings:"
msgstr "Configurações adicionais:"

#: contents/ui/config/configGeneral.qml:54
#, kde-format
msgid "Enable Notifications"
msgstr "Ativar notificações"

#: contents/ui/config/configGeneral.qml:60
#, kde-format
msgid "Enable Remote TTS"
msgstr "Ativar TTS remoto"

#: contents/ui/FullRepresentation.qml:85
#, kde-format
msgid "Conversation"
msgstr "Conversa"

#: contents/ui/FullRepresentation.qml:91
#, kde-format
msgid "Hints & Tips"
msgstr "Dicas e truques"

#: contents/ui/FullRepresentation.qml:97
#, kde-format
msgid "Skill Browser"
msgstr "Navegador de habilidades"

#: contents/ui/SkillsInstallerComponent.qml:98
#, kde-format
msgid "Search Skills"
msgstr "Pesquisar habilidades"

#: contents/ui/SkillsInstallerComponent.qml:116
#, kde-format
msgid "Refresh List"
msgstr "Atualizar lista"

#: contents/ui/TopBarViewComponent.qml:73
#, kde-format
msgid "Mycroft"
msgstr "Mycroft"

#: contents/ui/TopBarViewComponent.qml:99
#, kde-format
msgid "Disconnected"
msgstr "Desconectado"

#: contents/ui/TopBarViewComponent.qml:120
#, kde-format
msgid "Connect"
msgstr "Conectar"

#: contents/ui/TopBarViewComponent.qml:138
#, kde-format
msgid "Toggle Mic"
msgstr "Alternar microfone"

#~ msgid "Enable Remote STT"
#~ msgstr "Ativar STT remoto"
