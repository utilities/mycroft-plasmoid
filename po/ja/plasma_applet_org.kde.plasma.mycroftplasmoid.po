msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.mycroftplasmoid\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:41+0000\n"
"PO-Revision-Date: 2017-12-09 19:30-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: contents/config/config.qml:25
#, kde-format
msgid "General"
msgstr ""

#: contents/ui/BottomBarViewComponent.qml:71
#, kde-format
msgid "Enter Query or Say 'Hey Mycroft'"
msgstr ""

#: contents/ui/config/configGeneral.qml:45
#, kde-format
msgid "Websocket Address:"
msgstr ""

#: contents/ui/config/configGeneral.qml:53
#, kde-format
msgid "Additional Settings:"
msgstr ""

#: contents/ui/config/configGeneral.qml:54
#, kde-format
msgid "Enable Notifications"
msgstr ""

#: contents/ui/config/configGeneral.qml:60
#, kde-format
msgid "Enable Remote TTS"
msgstr ""

#: contents/ui/FullRepresentation.qml:85
#, kde-format
msgid "Conversation"
msgstr ""

#: contents/ui/FullRepresentation.qml:91
#, kde-format
msgid "Hints & Tips"
msgstr ""

#: contents/ui/FullRepresentation.qml:97
#, kde-format
msgid "Skill Browser"
msgstr ""

#: contents/ui/SkillsInstallerComponent.qml:98
#, kde-format
msgid "Search Skills"
msgstr ""

#: contents/ui/SkillsInstallerComponent.qml:116
#, kde-format
msgid "Refresh List"
msgstr ""

#: contents/ui/TopBarViewComponent.qml:73
#, kde-format
msgid "Mycroft"
msgstr ""

#: contents/ui/TopBarViewComponent.qml:99
#, kde-format
msgid "Disconnected"
msgstr ""

#: contents/ui/TopBarViewComponent.qml:120
#, kde-format
msgid "Connect"
msgstr ""

#: contents/ui/TopBarViewComponent.qml:138
#, kde-format
msgid "Toggle Mic"
msgstr ""
